/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package padrao;

/**
 *
 * @author Silvarney Henrique
 */
public class CelularAdapter implements Dispositivo{
    
    private CelularAndroid celularAndroid;

    public CelularAdapter(CelularAndroid celularAndroid) {
        this.celularAndroid = celularAndroid;
    }
    
    @Override
    public void iniciar() {
        celularAndroid.ligarDispositivo();
        celularAndroid.procurarRede();
    }

    @Override
    public void desligar() {
        celularAndroid.desconectarRede();
        celularAndroid.desligarDispositivo();
    }

    @Override
    public void reiniciar() {
        desligar();
        iniciar();
    }
    
}

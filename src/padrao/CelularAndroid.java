/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package padrao;

/**
 *
 * @author Silvarney Henrique
 */
public class CelularAndroid {
    public void ligarDispositivo(){
        System.out.println("Celular: ligar dispositivo");
    }
    
    public void procurarRede(){
        System.out.println("Celular: procurar rede");
    }
    
    public void desligarDispositivo(){
        System.out.println("Celular: desligar dispositivo");
    }
    
    public void desconectarRede(){
        System.out.println("Celular: desconectar rede");
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package padrao;

/**
 *
 * @author Silvarney Henrique
 */
public class Notebook implements Dispositivo{

    @Override
    public void iniciar() {
        System.out.println("Notebook: iniciar");
    }

    @Override
    public void desligar() {
        System.out.println("Notebook: desligar");
    }

    @Override
    public void reiniciar() {
        this.desligar();
        this.iniciar();
    }
    
}

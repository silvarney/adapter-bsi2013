/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package padrao;

/**
 *
 * @author Silvarney Henrique
 */
public class Aplicacao {
    public static void main(String[] args) {
        //Notebook é o nosso exemplo base 
        Dispositivo n = new Notebook();
        n.reiniciar();
        
        //Utilizamos o adapter no celular já que ele tem muitas funçoes e diferente da nossa base
        CelularAndroid celularAndroid = new CelularAndroid();
        Dispositivo d = new CelularAdapter(new CelularAndroid());
        d.reiniciar();
        
    }
}
